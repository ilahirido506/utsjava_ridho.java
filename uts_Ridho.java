import java.util.Scanner;

public class PointOfSale {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Inisialisasi data produk dalam array
        String[] produk = {"Produk A", "Produk B", "Produk C"};
        double[] harga = {10.0, 15.0, 20.0};
        int[] stok = {50, 30, 25};

        System.out.println("Selamat datang di Point of Sale!");
        boolean runProgram = true;
        double totalBelanja = 0;

        while (runProgram) {
            // Menampilkan menu produk
            System.out.println("\nDaftar Produk:");
            for (int i = 0; i < produk.length; i++) {
                System.out.println(i + ". " + produk[i] + " - Harga: $" + harga[i] + " - Stok: " + stok[i]);
            }

            System.out.print("Pilih produk (0 untuk keluar): ");
            int pilihan = scanner.nextInt();

            if (pilihan >= 0 && pilihan < produk.length) {
                int qty;
                do {
                    System.out.print("Jumlah yang ingin dibeli: ");
                    qty = scanner.nextInt();
                } while (qty < 0 || qty > stok[pilihan]);

                if (qty > 0) {
                    double subtotal = harga[pilihan] * qty;
                    totalBelanja += subtotal;
                    stok[pilihan] -= qty;
                    System.out.println("Produk " + produk[pilihan] + " sejumlah " + qty + " buah berhasil ditambahkan ke keranjang.");
                    System.out.println("Total Belanja Anda saat ini: $" + totalBelanja);
                }
            } else if (pilihan == 0) {
                runProgram = false;
            } else {
                System.out.println("Pilihan tidak valid. Silakan coba lagi.");
            }
        }

        System.out.println("Terima kasih sudah berbelanja!");
        System.out.println("Total Belanja Anda: $" + totalBelanja);

        scanner.close();
    }
}s
